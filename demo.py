from flask import Flask
from post import Post
app = Flask(__name__)

posts=[Post("hello",1,"me","mememem","woah"),Post("bye",2,"not me","notmemememe","not woah")]

@app.route('/')
def index():
    return 'Jello World!'

@app.route('/post/')
def get_posts():
    posting=""
    for post in posts:
        posting+=f'{post}'
    return posting

@app.route('/post/<int:id>')
def get_post(id):
    for post in posts:
        if id == post.id:
            return f'{post}'
                
                
