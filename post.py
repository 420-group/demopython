class Post:
    def __init__(self,title,id,author,text,tag_line):
        self.title=title
        self.id=id
        self.author=author
        self.text=text
        self.tag_line=tag_line
        
    def __str__(self):
        return f'Title: {self.title}, Id: {self.id}, Author: {self.author}, Text: {self.text}, Tagline: {self.tag_line}'
        
class ImagePost(Post):
    
    def __init__(self,title,id,author,text,tag_line,images,path):
        super().__init__(self,title,id,author,text,tag_line)
        self.images=images
        self.path=path
        self.__index = 0
    def __iter__(self):
        return self
    def __next__(self):
        if self.__index == len(self.images):
            raise StopIteration
        first = self.images[self.__index]
        self.__index += 1
        return first
        