# Description
class demo app for python, server side programming

# Setup
1. Create virtual environment (python -m venv .venv)
2. Activate virtual environment (source ./venv/Scripts/activate){if in gitbash} or (./venv/Scripts/activate)
3. Install requirements (pip install -r requirements.txt)

# Run or Debugging
1. flask run